const { GObject, Gtk, Handy, Json, Gdk, Gio, GLib, WebKit2 } = imports.gi;

Handy.init();

var sc = new Json.Array();
var num = 1;
var blocklist = {
  'Move Forward': {
    params: [[57, '']],
    color: 'red',
    type: 23
  },
  'Turn': {
    params: [[57, 'degrees']],
    color: 'red',
    type: 24
  },
  'Change X': {
    params: [[57, 'by']],
    color: 'red',
    type: 27
  },
  'Change Y': {
    params: [[57, 'by']],
    color: 'red',
    type: 28
  },
  'Set Position': {
    params: [[57, 'to x'], [57, 'y']],
    color: 'red',
    type: 41
  },
  'Set Angle': {
    params: [[57, '']],
    color: 'red',
    type: 39
  },
  'Flip': {
    params: [],
    color: 'red',
    type: 50
  },
  'Set Speed': {
    params: [[57, 'to']],
    color: 'red',
    type: 34
  },
  'Set Origin': {
    params: [[57, 'to x'], [57, 'y']],
    color: 'red',
    type: 59
  },
  'separate': '',
  'Set Color': {
    params: [[44, '']],
    color: 'green',
    type: 54
  }
}

var HsWindow = GObject.registerClass({
    GTypeName: 'HsWindow',
    Template: 'resource:///com/gethopscotch/Hopscotch/window.ui',
    InternalChildren: ['stack', 'blockgrid', 'news', 'scn', 'objn', 'sn', 'newsc', 'scenes', 'newobj', 'obx', 'oby', 'obn', 'objsstack', 'aru', 'adru', 'rules', 'blocks', 'exp', 'pti', 'pwi', 'phe', 'pucre', 'puid', 'puav', 'pusr', 'json', 'menu']
}, class HsWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ application });
        var cp = new Gtk.CssProvider();
        cp.load_from_resource('/com/gethopscotch/Hopscotch/js/style.css');
        Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(), cp, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        const PERMISSIONS_MODE = 0o744;

        this._news.connect('clicked', () => this._scn.popup());

        var current={x:1,y:1}
        Object.keys(blocklist).forEach(v => {
          if(v === 'separate') {
            if(current.y == 2) current.x += 1;
            current.y = 1;
            var myb = Gtk.Separator.new(1);
            myb.name = 'sep';
            this._blockgrid.attach(myb, current.x, 1, 1, 2);
            current.x += 1;
          } else {
            var myb = Gtk.Button.new_with_label(v);
            this._blockgrid.attach(myb, current.x, current.y, 1, 1);
            if(current.y == 2) {
              current.x += 1;
              current.y = 1;
            } else {
              current.y += 1;
            }
          }
          this._blockgrid.show_all();
        })

        this._exp.connect('clicked', () => {
          var exported = this.export();
          var p = Gtk.FileChooserNative.new(_('Save your project'), this, Gtk.FileChooserAction.SAVE, _('Save'), _('Cancel'));
          p.do_overwrite_confirmation = true;
          if(p.run() == Gtk.ResponseType.ACCEPT) {
            let path = GLib.build_filenamev([p.get_filename()]);
            let file = Gio.File.new_for_path(path);
            if (GLib.mkdir_with_parents(file.get_parent().get_path(), PERMISSIONS_MODE) === 0) {
              file.replace_contents(
                exported,
                null,
                false,
                Gio.FileCreateFlags.REPLACE_DESTINATION,
                null
              );
            }
            this._popover1.popdown();
          }
        });

        this._stack.connect('notify::visible-child', (stack) => {
          if(stack.visible_child_name == 'editor') {
            var bf = new Gtk.TextBuffer();
            bf.set_text(this.export(), this.export().length)
            this._json.buffer = bf;
          }
        })

        this._newsc.set_sensitive(false);
        this._sn.connect('changed', () => {
          if(sc.get_elements().map((a) => { a = a.dup_object(); return a.get_string_member('name'); }).includes(this._sn.get_text()) || this._sn.get_text().length < 1) {
            this._newsc.set_sensitive(false);
          } else {
            this._newsc.set_sensitive(true);
          }
        });

        this._newobj.set_sensitive(false);
        this._obx.connect('changed', () => {
          if(/^\d+$/.test(this._obx.get_text()) && /^\d+$/.test(this._oby.get_text())) {
            this._newobj.set_sensitive(true);
          } else {
            this._newobj.set_sensitive(false);
          }
        });
        this._oby.connect('changed', () => {
          if(/^\d+$/.test(this._obx.get_text()) && /^\d+$/.test(this._oby.get_text())) {
            this._newobj.set_sensitive(true);
          } else {
            this._newobj.set_sensitive(false);
          }
        });

        this._pwi.connect('changed', () => {
          if(/^\d+$/.test(this._pwi.get_text()) && /^\d+$/.test(this._phe.get_text())) {
            this._exp.set_sensitive(true);
          } else {
            this._exp.set_sensitive(false);
          }
        });
        this._phe.connect('changed', () => {
          if(/^\d+$/.test(this._pwi.get_text()) && /^\d+$/.test(this._phe.get_text())) {
            this._exp.set_sensitive(true);
          } else {
            this._exp.set_sensitive(false);
          }
        });


        // random nonsense thing
        this.str = '';
        for(var x = 0; x < 31; x++) {
          if([4, 9, 14].includes(x)) {
            this.str += '-';
          } else {
            this.str += this.random();
          }
        }

        this._sn.connect('activate', () => {
          if(sc.get_elements().map((a) => { a = a.dup_object(); return a.get_string_member('name'); }).includes(this._sn.get_text()) || this._sn.get_text().length < 1) return;
          this._newsc.clicked()
        });

        this._newsc.connect('clicked', () => {
          var self = false;
          var min = sc.get_elements().length;
          var a = Gtk.Box.new(1, 5);
          var b = Gtk.Label.new('Scene: ' + this._sn.get_text());
          b.selectable = true;
          b.set_halign(1);
          b.margin_start = b.margin_end = a.margin_top = a.margin_start = a.margin_end = 10;
          a.pack_start(b, false, true, 0);
          a.get_style_context().add_class('scene');
          var c = new Gtk.Button();
          c.connect('clicked', () => {
            this._objn.set_relative_to(c);
            this._objn.popup();
            self = true;
            this._obn.grab_focus_without_selecting();
            this._newobj.connect('clicked', () => {
              if(self) {
                var newo = new Json.Object();
                var t = sc.get_object_element(min).get_array_member('objects').get_elements().length;
                newo.set_string_member('type', this._objsstack.visible_child_name);
                newo.set_int_member('x', +this._obx.get_text());
                newo.set_int_member('y', +this._oby.get_text());
                newo.set_string_member('name', this._obn.get_text());
                newo.set_array_member('rules', new Json.Array());
                newo.set_int_member('num', num);
                sc.get_object_element(min).get_array_member('objects').add_object_element(newo);
                this.addObj(min, t, a, this._objsstack.get_visible_child().label);
                this._objn.popdown();
                this._obx.text = this._oby.text = this._obn.text = '';
              }
            });
            this._objn.connect('closed', () => {
              self = false;
            })
          });
          c.set_relief(2);
          var d = Gtk.Box.new(0, 0);
          c.add(d);
          var e = Gtk.Image.new_from_icon_name('list-add-symbolic', 2);
          d.pack_start(e, false, true, 0);
          var f = Gtk.Label.new(_('Add Object'));
          f.tooltip_text = _('Create a New Object');
          d.pack_start(f, true, true, 0);
          a.pack_start(c, false, true, 0);
          c.margin_start = c.margin_end = 10;
          this._scenes.pack_start(a, false, true, 0);
          this._scenes.show_all();
          var g = new Json.Object();
          g.set_string_member('name', this._sn.get_text());
          g.set_array_member('objects', new Json.Array());
          sc.add_object_element(g);
          this._scn.popdown();
          this._sn.set_text('');
          num += 1;
        });
    }

    addObj(ia, ib, ic, id) {
      var selo = sc.get_object_element(ia).get_array_member('objects').get_object_element(ib);
      var self = false;
      var a = Gtk.Box.new(1, 5);
      a.get_style_context().add_class('object');
      var b = Gtk.Label.new(`Object: ${(selo.get_string_member('name').length < 1) ? `❔ ${num}` : selo.get_string_member('name')} (${id})`);
      b.selectable = true;
      b.set_halign(1);
      b.margin_start = b.margin_end = a.margin_top = 10;
      a.pack_start(b, false, true, 0);
      ic.pack_start(a, false, true, 0);
      var f = Gtk.Box.new(1, 5);
      var c = Gtk.Button.new_with_label(_('Add a rule'));
      c.set_relief(2);
      c.connect('clicked', () => {
        this._aru.set_relative_to(c);
        this._aru.popup();
        self = true;
        this._adru.connect('clicked', () => {
          if(self) {
            var newo = new Json.Object();
            newo.set_string_member('type', this._rules.visible_child_name);
            newo.set_string_member('show', this._rules.get_visible_child().label);
            var params = new Json.Array();
            newo.set_array_member('params', params);
            newo.set_array_member('blocks', new Json.Array());
            selo.get_array_member('rules').add_object_element(newo);
            self = false;
            this.addRule(ia, ib, selo.get_array_member('rules').get_elements().length - 1, a);
            this._aru.popdown();
          }
        });
      });
      c.margin_start = c.margin_end = 10;
      a.pack_start(c, false, true, 0);
      a.pack_start(f, false, true, 0);
      ic.show_all();
      num += 1;
    }

    addRule(ia, ib, ic, id) {
      var self = false;
      var selo = sc.get_object_element(ia).get_array_member('objects').get_object_element(ib).get_array_member('rules').get_object_element(ic);
      var a = Gtk.Box.new(1, 5);
      a.get_style_context().add_class('rule');
      var b = Gtk.Label.new(`Rule: When ${selo.get_string_member('show')}`);
      b.set_halign(1);
      b.margin_start = b.margin_end = a.margin_top = 10;
      a.pack_start(b, false, true, 0);
      var c = Gtk.Button.new_with_label(_('Add block'));
      c.set_relief(2);
      c.margin_start = c.margin_end = 10;
      c.connect('clicked', () => {
        self = true;
        this._blocks.set_relative_to(c);
        this._blocks.popup();
      });
      var d = Gtk.Box.new(1, 6);
      a.pack_start(d, false, true, 0);
      this._blocks.get_children()[0].get_children()[0].get_children()[0].get_children().forEach(v => {
        if(v.name == 'sep') return;
        v.connect('clicked', () => {
          if(self) {
            var newb = new Json.Object();
            newb.set_object_member('params', new Json.Object());
            newb.set_string_member('show', v.label);
            var prnt = Gtk.ScrolledWindow.new(null, null);
            prnt.vscrollbar_policy = 2;
            newb.set_int_member('type', blocklist[v.label].type);
            var h = Gtk.Box.new(0, 8);
            prnt.add(h);
            var g = Gtk.Label.new(v.label);
            h.pack_start(g, false, true, 0);
            h.margin_start = h.margin_end = 10;
            h.get_style_context().add_class('block');
            h.get_style_context().add_class(blocklist[v.label].color);
            var pind = 0;
            blocklist[v.label].params.forEach(v => {
              if(v[0] == 57) {
                var p = new Gtk.Entry();
                p.get_style_context().add_class('param');
                p.placeholder_text = v[1];
                h.pack_start(p, false, false, 0);
                var k = new Json.Object();
                k.set_string_member('defaultValue', '10');
                k.set_int_member('type', 57);
                k.set_string_member('value', '');
                k.set_string_member('key', '');
                k.set_int_member('pind', pind);
                newb.get_object_member('params').set_object_member(pind+'', k);
                p.connect('changed', () => {
                  newb.get_object_member('params').get_object_member(k.get_int_member('pind')+'').set_string_member('value', p.get_text());
                });
                pind+=1;
              } else if(v[0] == 44) {
                var p = Gtk.ColorButton.new();
                p.use_alpha = false;
                p.set_title('Color')
                h.pack_start(p, false, false, 0);
                var k = new Json.Object();
                k.set_string_member('defaultValue', 'HSB(0,0,100)');
                k.set_int_member('type', 44);
                k.set_string_member('value', 'HSB(0,0,100)');
                k.set_string_member('key', '');
                k.set_int_member('pind', pind);
                newb.get_object_member('params').set_object_member(pind+'', k);
                p.connect('color-set', () => {
                  var res = p.get_rgba()
                  res = Gtk.rgb_to_hsv(res.red, res.green, res.blue);
                  res = `HSB(${res[0]*100},${res[1]*100},${res[2]*100})`;
                  newb.get_object_member('params').get_object_member(k.get_int_member('pind')+'').set_string_member('value', res);
                });
                pind+=1;
              }
            })
            selo.get_array_member('blocks').add_object_element(newb);
            d.pack_start(prnt, false, true, 0);
            d.show_all();
            this._blocks.popdown();
          }
        })
      })
      this._blocks.connect('closed', () => self = false);
      a.pack_start(c, false, true, 0);
      id.pack_start(a, false, true, 0);
      id.show_all();
    }

    random() {
      var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
      return characters.charAt(Math.floor(Math.random() * characters.length))
    }

    export() {
      var gener = new Json.Object();
      gener.set_string_member('title', this._pti.get_text());
      var st = new Json.Object();
      st.set_int_member('width', +this._pwi.get_text());
      st.set_int_member('height', +this._phe.get_text());
      gener.set_object_member('stageSize', st);
      var a = '';
      for(var b = 0; b < 8; b++) {
        a += this.random();
      }
      var us = new Json.Object();
      us.set_string_member('nickname', this._pusr.get_text());
      us.set_int_member('id', +this._puid.get_text());
      us.set_int_member('avatar_type', +this._puav.get_text());
      us.set_string_member('created_at', this._pucre.get_text());
      us.set_boolean_member('iphone_user', false);
      gener.set_string_member('uuid', a);
      gener.set_object_member('user', us);
      gener.set_object_member('original_user', us);
      gener.set_string_member('author', 'me');
      gener.set_array_member('custom_objects', new Json.Array());
      gener.set_string_member('playerVersion', '1.5.0');
      gener.set_int_member('version', 32);

      gener.set_array_member('variables', new Json.Array());
      gener.set_array_member('custom_rules', new Json.Array());
      gener.set_array_member('remote_asset_urls', new Json.Array());
      gener.set_array_member('eventParameters', new Json.Array());
      gener.set_int_member('fontSize', 80);
      gener.set_object_member('playerUpgrades', new Json.Object());

      var scen = new Json.Array();
      var obj = new Json.Array();
      var rule = new Json.Array();
      var abil = new Json.Array();
      sc.get_elements().forEach(v => {
        v = v.dup_object();
        var e = new Json.Object();
        e.set_string_member('name', v.get_string_member('name'));
        var f = new Json.Array();

        v.get_array_member('objects').get_elements().forEach(va => {
          va = va.dup_object();
          var i = new Json.Object();
          var w = '';
          for(var x = 0; x < 58; x++) {
            if([9, 14, 19, 24, 37, 42].includes(x)) {
              w += '-';
            } else {
              w += this.random();
            }
          }
          f.add_string_element(w);
          i.set_string_member('objectID', w);
          i.set_string_member('name', va.get_string_member('name'));
          i.set_string_member('xPosition', ''+va.get_int_member('x'));
          i.set_string_member('yPosition', ''+va.get_int_member('y'));
          i.set_string_member('resizeScale', '1');
          i.set_string_member('height', '150');
          i.set_string_member('width', '150');

          switch(va.get_string_member('type')) {
            case 'square':
              i.set_int_member('type', 34);
              break;
            case 'circle':
              i.set_int_member('type', 35);
              break;
            case 'rect':
              i.set_int_member('type', 39);
          }

          var rul = new Json.Array();

          va.get_array_member('rules').get_elements().forEach(vb => {
            vb = vb.dup_object();
            var e = new Json.Object();
            var q = '';
            for(var x = 0; x < 58; x++) {
              if([9, 14, 19, 24, 37, 42].includes(x)) {
                q += '-';
              } else {
                q += this.random();
              }
            }
            rul.add_string_element(q);

            e.set_string_member('id', q);
            e.set_string_member('objectID', w)
            e.set_string_member('name', '');
            e.set_int_member('ruleBlockType', 6000);
            e.set_int_member('type', 6000);
            var ar = new Json.Array();
            var ob = new Json.Object();
            ob.set_string_member('defaultValue', '');
            ob.set_string_member('key', '');
            ob.set_string_member('value', '');
            ob.set_int_member('type', 52);
            var dat = new Json.Object();
            switch(vb.get_string_member('type')) {
              case 'stg':
                dat.set_int_member('type', 7000);
                break;
              case 'gp':
                dat.set_int_member('type', 7021);
            }
            dat.set_string_member('block_class', 'operator');
            dat.set_string_member('description', vb.get_string_member('show'));
            ob.set_object_member('datum', dat);
            ar.add_object_element(ob);
            e.set_array_member('parameters', ar);

            var s = '';
            var bl = new Json.Object();
            var bls = new Json.Array();
            for(var x = 0; x < 58; x++) {
              if([9, 14, 19, 24, 37, 42].includes(x)) {
                s += '-';
              } else {
                s += this.random();
              }
            }
            bl.set_string_member('abilityID', s);
            e.set_string_member('abilityID', s);
            vb.get_array_member('blocks').get_elements().forEach(vc => {
              vc = vc.dup_object();
              var abi = new Json.Object();
              abi.set_int_member('type', vc.get_int_member('type'));
              abi.set_string_member('block_class', 'method');
              abi.set_string_member('description', vc.get_string_member('show'));
              var par = new Json.Array();
              vc.get_object_member('params').get_values().forEach(vd => {
                vd = vd.dup_object();
                par.add_object_element(vd);
              });
              abi.set_array_member('parameters', par);
              bls.add_object_element(abi);
            });
            bl.set_array_member('blocks', bls);

            rule.add_object_element(e);
            abil.add_object_element(bl);
          });

          i.set_array_member('rules', rul);

          //TODO: add text once text object is added
          i.set_string_member('text', '');

          obj.add_object_element(i);
        });

        e.set_array_member('objects', f);

        scen.add_object_element(e);
      });
      gener.set_array_member('scenes', scen);
      gener.set_array_member('objects', obj);
      gener.set_array_member('rules', rule);
      gener.set_array_member('abilities', abil);

      var node = Json.Node.new(0).init_object(gener);
      var exported = Json.to_string(node, true);
      return exported;
    }
});
